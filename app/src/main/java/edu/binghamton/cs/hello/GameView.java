package edu.binghamton.cs.hello;

/**
 * Created by pmadden on 2/23/16.
 */

import android.annotation.SuppressLint;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import java.util.Timer;
import java.util.TimerTask;

public class GameView extends View {
    private int mClicks, tapped;
    private float lastX, lastY;
    private Paint mBorderPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private Paint mCirclePaint = new Paint();
    private Timer mTimer;
    private TimerTask mTask;
    private Canvas mCanvas;
    private View self;

    public void start()
    {
        tapped = 1;
        mBorderPaint.setColor(0xFFCCBB00);
    }

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mClicks = 0;
        tapped = 0;
        lastX = lastY = -1;
        mBorderPaint.setColor(0xFFFF0000);
        mTextPaint.setColor(0xFF101010);
        mCirclePaint.setColor(0xFFFFFFFF);
        self = this;

        // mTask = new TimerScheduleFixedRate();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Method
                self.postInvalidate();
                mClicks = mClicks + 1;

                switch (mClicks % 4) {
                    case 0:
                        mBorderPaint.setColor(0xFFFF0000);
                        break;
                    case 1:
                        mBorderPaint.setColor(0xFFFFFF00);
                        break;
                    case 2:
                        mBorderPaint.setColor(0xFFFF00FF);
                        break;
                    case 3:
                        mBorderPaint.setColor(0xFF00FF00);
                        break;

                }
            }
        }, 0, 500);

        // mTimer.scheduleAtFixedRate();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            int index = event.getActionIndex();
            lastX = event.getX(index);
            lastY = event.getY(index);
            this.postInvalidate();
        }
        return true;
    }
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(20, 20, getWidth() - 20, getHeight() - 20, mBorderPaint);
        if (lastX > 0) {
            canvas.drawCircle(lastX, lastY, 100, mCirclePaint);
            canvas.drawText("CS441", lastX, lastY, mTextPaint);

        }
    }
}
